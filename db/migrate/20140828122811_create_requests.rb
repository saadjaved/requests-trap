class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.string :remote_ip
      t.string :request_method
      t.string :scheme
      t.text :query_string
      t.text :query_params
      t.text :cookies
      t.text :headers
      t.integer :user_id

      t.timestamps
    end
  end
end
