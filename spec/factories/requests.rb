# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :request do
    remote_ip "127.0.0.1"
    request_method "GET"
    scheme "HTTP"
    query_string ""
    query_params ""
    cookies ""
    headers ""
    user
  end
end
