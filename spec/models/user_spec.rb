require 'rails_helper'

RSpec.describe User, :type => :model do

  before(:each) do
    @user = FactoryGirl.create(:user)
    user_requests = []
    user_requests << FactoryGirl.create(:request, user: @user)
    user_requests << FactoryGirl.create(:request, user: @user)
    user_requests << FactoryGirl.create(:request, user: @user)
  end

  it "should create a new instance of a user given valid attributes" do
    FactoryGirl.create(:user, email: 'test@gmail.com').should be_valid
  end

  it "should validate password confirmation" do
    FactoryGirl.build(:user, password_confirmation: '2582').should_not be_valid
  end

  it "should validate duplicate email" do
    FactoryGirl.create(:user, email: "adele@gmail.com")
    FactoryGirl.build(:user, email: "adele@gmail.com").should_not be_valid
  end

  it "should validate presence of email" do
    FactoryGirl.build(:user, email: nil).should_not be_valid
  end

  it 'should return user requests by created at desc' do
    requests = User.get_user_requests(@user.slug)
    expect(requests.first.created_at.should be > requests.last.created_at)
  end

  it 'should return a valid user instance given trap_id' do
    user = User.get_user_info(@user.slug)
    expect(user).to be_valid
  end

  it 'should generate a valid trap_id' do
    user = FactoryGirl.create(:user, email: 'username@yahoo.com')
    user.slug.should eq 'username-yahoo-com'
  end

  it 'should throw an exception given invalid trap_id' do
    user = FactoryGirl.build(:user, slug: 'test')
    expect{User.get_user_info(user.slug)}.to raise_error
  end
end
