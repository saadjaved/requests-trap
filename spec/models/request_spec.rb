require 'rails_helper'

RSpec.describe Request, :type => :model do

  it "should create a new instance of request given valid attributes" do
    FactoryGirl.create(:request).should be_valid
  end

  it 'should validate presence of remote ip' do
    FactoryGirl.build(:request, remote_ip: nil).should_not be_valid
  end

  it 'should validate presence of request method' do
    FactoryGirl.build(:request, request_method: nil).should_not be_valid
  end

  it 'should validate presence of scheme' do
    FactoryGirl.build(:request, scheme: nil).should_not be_valid
  end

  it 'should create a valid instance of a request given remote ip' do
    FactoryGirl.build(:request, remote_ip: '127.0.0.1').should be_valid
  end

  it 'should create a valid instance of a request given request method' do
    FactoryGirl.build(:request, request_method: 'GET').should be_valid
  end

  it 'should create a valid instance of a request given scheme' do
    FactoryGirl.build(:request, scheme: 'HTTP').should be_valid
  end
end
