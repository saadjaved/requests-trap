class Request < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :remote_ip, :request_method, :scheme
  serialize :cookies
  serialize :headers

  def self.get_latest_requests(trap_id, request_id)
    user = User.get_user_info(trap_id)
    user.requests.where('id > ?', request_id).order('created_at DESC') rescue nil
  end
end
