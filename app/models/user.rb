class User < ActiveRecord::Base
  extend FriendlyId
  friendly_id :email, use: :slugged
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :requests

  def self.get_user_requests(trap_id)
    user = get_user_info(trap_id)
    user.requests.order('created_at DESC') rescue nil
  end

  def self.get_user_info(trap_id)
    self.friendly.find(trap_id)
  end
end
