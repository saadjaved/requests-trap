class RequestsController < ApplicationController
  before_filter :authenticate_user!, :only => [:dashboard, :display_requests_listing]
  skip_before_filter  :verify_authenticity_token, except: [:index, :show, :dashboard, :display_requests_listing]

  def index
    request_flag = add_request_details
    respond_to do |format|
      format.html
      format.json {
        if request_flag
          render nothing: true, status: :ok
        else
          render nothing: true, status: :internal_server_error
        end
      }
    end
  end

  def show
    begin
      @request = current_user.requests.find(params[:id])
    rescue => exception
      redirect_to display_requests_listing_url
    end
  end

  def create
    request_flag = add_request_details
    if request_flag
      render nothing: true, status: :ok
    else
      render nothing: true, status: :internal_server_error
    end
  end

  def update
    request_flag = add_request_details
    if request_flag
      render nothing: true, status: :ok
    else
      render nothing: true, status: :internal_server_error
    end
  end

  def destroy
    request_flag = add_request_details
    if request_flag
      render nothing: true, status: :ok
    else
      render nothing: true, status: :internal_server_error
    end
  end

  def dashboard
  end

  def display_requests_listing
    @users_requests = User.get_user_requests(params[:trap_id])
  end

  def fetch_user_requests
    max_id = params[:id]
    @user_requests = Request.get_latest_requests(current_user.slug, max_id)
    @max = @user_requests.first.id rescue nil
    render partial: '/requests/fetch_request', layout: false
  end

  private

  def add_request_details
    begin
      user = User.get_user_info(params[:trap_id])
      req = Request.new(user_id: user.id,
                        remote_ip: request.remote_ip,
                        request_method: request.request_method,
                        scheme:request.scheme,
                        query_string: request.query_string.to_s,
                        query_params: request.query_parameters.to_s,
                        cookies: request.cookies,
                        headers: request.headers.env.to_s)
      req.save ? true : false
    rescue => exception
      puts "Exception details: #{exception.message.to_s}"
      flag = false
    end
  end
end
