# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
(poll = ->
  setTimeout (->
    formData = id: $("#max_request_id").html()
    $.ajax
      url: "/requests/fetch_user_requests"
      type: "POST"
      data: formData
      success: (response) ->
        $('#requests_listing tbody').append(response)
        $("#max_request_id").text max  if max > 0
        return

      complete: poll
      timeout: 20000

    return
  ), 10000
  return
)()
return