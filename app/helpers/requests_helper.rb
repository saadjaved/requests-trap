module RequestsHelper
  def get_max_request(requests)
    requests.first.id rescue 0
  end
  def get_user_slug(request)
    request.user.slug
  end
end
